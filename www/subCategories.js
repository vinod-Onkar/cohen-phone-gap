Application.SubCategories = function SubCategories()
{
    this.subCatlist = this.getSubCategaries(); 
}

Application.SubCategories.prototype.getSubCategaries = function()
{
   // Server data fetch or local db fetch     
     // All code here     
    return Application.DataSource.SubCategories;  
}

Application.SubCategories.prototype.showSubCategories = function(id)
{ 
 var listArray = [];

 for(var i=0;i<this.subCatlist.length;i++)
  { 
     if(this.subCatlist[i].id == id)
     {
        listArray = this.subCatlist[i].subCategories;
     } 
  } 
  return listArray;
}