Application.Components.ListView =  function(options){
	this.container = options.container || "div";
	this.className = options.className || "";
	this.data = options.data;
	this.template = options.template;
};


Application.Components.ListView.prototype.Render =  function(){
	var container = document.createElement(this.container);
	$(container).addClass(this.className);
	var itemView;
	for(var item=0;item < this.data.length;item++){
		itemView = _.template(this.template, this.data[item]);
		$(container).append(itemView);
	}
	return $(container);
};
Application.Components.ListView.prototype.Remove =  function(){
	console.log("Remove List");return this;
};
