/***************************************************************************/
// App Structure
var Application = {};
Application.Components 	= {};
Application.Database 	= {};
Application.Events 		= {};
Application.Classes 	= {};
Application.Utilities 	= {};
/***************************************************************************/



/***************************************************************************/
// Components 
/***************************************************************************/
// 1. Listview to render lists
Application.Components.ListView =  function(options){
	this.data = options.data;
	this.template = options.template;
};
Application.Components.ListView.prototype.Render =  function(){console.log("Render List");return this;};
Application.Components.ListView.prototype.Remove =  function(){console.log("Remove List");return this;};
/***************************************************************************/
// 2. SelectList to render dropdowns
Application.Components.SelectList =  function(options){
	this.data = options.data;
	this.textField = options.textField;
	this.valueField = options.valueField;
	this.defaultSelection = options.defaultSelection;
};
Application.Components.SelectList.prototype.Render =  function(){console.log("Render Dropdown List");return this;};
Application.Components.SelectList.prototype.Remove =  function(){console.log("Render Dropdown List");return this;};
/***************************************************************************/


/***************************************************************************/
// Utilities 
/***************************************************************************/
Application.Utilities.FormatDate = function(Date, format){
	return "Formatted Date";
}
Application.Utilities.StripHtml = function(HtmlText){
	return "String after Stripping Html";
}
Application.Utilities.Validation = {
	ValidEmail : function(input){return true;},
	ValidPhone : function(input){return true;},
	ValidDate : function(input){return true;}
};

