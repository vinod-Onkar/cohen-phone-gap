var category = new Application.Categories();
var catList = category.showCategories();
     src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false";
/*****************************************************/
$(document).ready(function () {
        /********************************************************************************/
        
                       $("div.yard-detail").hide();
                  
            $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").toggleClass("open");
            $("#page-content-wrapper").toggleClass("shrink-content");
        });
        /********************************************************************************/
        $("document").click(function(e) {
            e.preventDefault();
            $("#sidebar-wrapper").toggleClass("open");
            $("#page-content-wrapper").toggleClass("shrink-content");
            $("#wrapper").toggleClass("active");
        });
        /********************************************************************************/
        //hide subcategories initially..
        $(".menu-back").attr("previous-menu","");
        //$("div.content-header").css("position","fixed");
        
        /********************************************************************************/
        // Populate Categories in main list
        for(var i=0;i<catList.length;i++)
        {
            var title = catList[i].title;
            var description = catList[i].description;
            $("h3").filter("#"+i).append(title);
            $("p").filter("#"+i).append(description);
        }
                  
        /********************************************************************************/
        //hide main div on click
        $("div.category").click(function(){

            var categoryID      = $(this).attr("div-id");
            var selectedClass   = $(this).attr("data-class");

            //get all subcategory headers..
            var allh4       = $(".subcategories h4");
            var subCategory = new Application.SubCategories();
            var subCatList  = subCategory.showSubCategories(categoryID);

            // Populate the static header first
            var selectedCategory= $("[data-id=" + categoryID + "]");
            var title           = selectedCategory.find("h3").html();
            var description     = selectedCategory.find("p").html();
            var categoryImage   = selectedCategory.find("img").attr("src");
            var borderColor = selectedCategory.find("div").css("border-left");
                                
            $(".subcategories .selected-category .title").html(title);
            $(".subcategories .selected-category .description").html(description);
            $(".subcategories .selected-category .category-image").attr("src", categoryImage);
             $(".subcategories .selected-category ").css("border-left", borderColor);
            
            // Append the Sub Categories for the category here
            var subcategoryTemplate = "<div class='panel sub-category-item " + selectedClass +"'><img class='img-responsive' src='images/circle2.png'><h4> </h4><img class='img' src='images/arrow.png'></div>";
            $(".subcategories .sub-category-list").empty();
            for(var i=0;i<subCatList.length;i++)
            {
                var subCategoryTitle = subCatList[i];
                var temp = $("<div/>").append(subcategoryTemplate);
                $(temp).find("h4").html(subCategoryTitle);
                $(".subcategories .sub-category-list").append($(temp).html());
            }
            // Hide the category list and display the subcategory list
            $("div.page-content").hide();
            $("div.subcategories").show();
             
            // Hide the Menu toggle icon and show the back button icon
            $("#menu-toggle").hide();
            $(".menu-back").attr("previous-menu","category").show();                           
        });
                  
        /********************************************************************************/
                  
        $(".sub-category-list .sub-category-item").live("click",function(){
            // 
            var detailTemplate = "<div class='sub-category-detail-item'><h4 class='title'>Cohen Middletown</h4><img src='images/house-icon.png' class='img'><h2 class='location'>Middletown</h2><img src='images/clock-icon.png' class='img'><h2 class='timing'>8:00AM-4:30PM</h2><img src='images/marker-icon.png' class='img'><h2 class='distance'>2.70mi</h2><span class='label label-success status'>OPEN</span></div>";
            $(".menu-back").attr("previous-menu","subcategory").show();
            $(".detail").empty();
            var compiledTemplate;
            for(var i=0;i<10;i++)
            {
                compiledTemplate = $("<div/>").append(detailTemplate);
                $(".detail").append(compiledTemplate.html());
            }
                                                        
            $("div.subcategories").hide();
            $(".detail").show();
            $(".detail").find(".sub-category-detail-item *").on("click",function(){
                $(".menu-back").attr("previous-menu","detail").show();
                $("div.detail").hide();
                $("div.yard-detail").show();
                //$(this).initializeMap();
            });

        });



    
                  
        /********************************************************************************/
                  
        $(".menu-back").click(function(){
            //
            var previousPage = $(this).attr("previous-menu");
            
            var menuConfig = {
                "category" : ".page-content",
                "subcategory" : ".subcategories",
                "detail" : ".detail"
            }

            var previousPageHolder = menuConfig[previousPage];
            $("#page-content-wrapper > div:not(.content-header)").hide();
            $(previousPageHolder).show();
            
            if(previousPage == "subcategory"){
                $(this).attr("previous-menu","category");
            }
                              
            if(previousPage == "category"){
                
                $("#menu-toggle").show();
                $(".menu-back").hide();
            }
//
            if(previousPage == "detail")
            {
                $(this).attr("previous-menu","subcategory");

            }
        });
                  
        /********************************************************************************/
                  
      // End of Document ready
    });

