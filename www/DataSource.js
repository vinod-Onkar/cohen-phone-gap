var Application = {};


Application.DataSource = {};
Application.Categories 	= {};
Application.SubCategories = {};
Application.Components = {};


Application.DataSource = {

	 Categories : [
     {
      title:'AUTOMOTIVE...',
      description : 'Dismantle with us for great value', 
      id:001
     },
    {
      title:'BATTERIES',
      description : 'Reusable or single use, either way proper disposal is important.',  
       id:002
    }, 
    {
      title:'ELECTRONICS',
      description : 'Dismantle with us for great value', 
       id:003
    },
    {
      title:'FERROUS METALS',
      description : 'Reusable or single use, either way proper disposal is important.',  
       id:004
    },
    {
      title:'NON-FERROUS METALS',
      description : 'Reusable or single use, either way proper disposal is important.',  
       id:005
    }  
  ],
  
   SubCategories : [
   {
     id :001,
     subCategories : ["AUTOMOTIVE1","AUTOMOTIVE2","AUTOMOTIVE3","AUTOMOTIVE4","AUTOMOTIVE5","AUTOMOTIVE6","AUTOMOTIVE7"],
   },
   {
     id :002,
     subCategories :["BATTERIES1","BATTERIES2","BATTERIES3","BATTERIES4","BATTERIES5","BATTERIES6","BATTERIES7"],
   },
   {
     id :003,
     subCategories : ["ELECTRONICS1","ELECTRONICS2","ELECTRONICS3","ELECTRONICS4","ELECTRONICS5","ELECTRONICS6","ELECTRONICS7"],
   },
   {
     id :004,
     subCategories : ["FERROUS METAL1","FERROUS METAL2","FERROUS METAL3","FERROUS METAL4","FERROUS METAL5","FERROUS METAL6","FERROUS METAL7"],
   },
   {
     id :005,
     subCategories : ["NON-FERROUS METAL1","NON-FERROUS METAL2","NON-FERROUS METAL3","NON-FERROUS METAL4","NON-FERROUS METAL5","NON-FERROUS METAL6","NON-FERROUS METAL7"],
   }
  
  ] 
}